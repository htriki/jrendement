
// Hocine Triki * 2021
// Ensemble des constantes

const   String ACHAT              = "achat";
const   String LOYER_MENSUEL      = "loyerMensuel";
const   String VACANCES           = "vacances";

// -- Impot / Defiscalisation ------------------------------------------------
const   String DEFISCALISATION    =" defiscalisation";
const   String IMPOT_ANNUEL       = "importAnnuel";

// -- Frais à l'achat --------------------------------------------------------
const   String NOTAIRE            = "notaire";
const   String TRAVAUX            = "travaux";
const   String MEUBLE             = "meuble";
const   String FRAIS_AGENCE       = "fraisAgence";
const   String COURTIER           = "courtier";
const   String AUTRE_FRAIS_ACHAT  = "autreFraisAchat";


// -- Charges à l'année ------------------------------------------------------
const   String TAXE_FONCIERE      = "taxeFonciere";
const   String ASSURANCE          = "assurance";          // Assurance Proprio
const   String ASSURANCE_IMPAYE   = "assuranceImpaye";
const   String GESTION_LOCATIVE   = "gestionLocative";
const   String COPROPRIETE        = "copropriete";
const   String ENTRETIEN          = "entretien";
const   String AUTRES_CHARGES     = "autresCharges";

// -- Financement ------------------------------------------------------------
const   String FI_MONTANT         = "FiMontant";
const   String FI_TAUX            = "FiTaux";
const   String FI_DUREE           = "FiDuree";
const   String FI_TAUX_ASS        = "FiTauxAss";
