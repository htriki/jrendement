import 'package:flutter/material.dart';
import 'package:jrendement/components/myBottomBar.dart';
import 'package:jrendement/components/myDrawer.dart';
import 'package:jrendement/components/myFloatingActionButton.dart';
import 'package:jrendement/components/mySection.dart';
import 'package:jrendement/components/mySliverAppBar.dart';
import 'package:jrendement/components/sectionAchat.dart';
import 'package:jrendement/components/sectionBoutton.dart';
import 'package:jrendement/components/sectionDefiscalisation.dart';
import 'package:jrendement/components/sectionFinancement.dart';
import 'package:jrendement/components/sectionFraisAchat.dart';
import 'package:jrendement/components/sectionFraisAnnee.dart';
import 'package:jrendement/components/sectionLoyer.dart';
import 'package:jrendement/components/sectionMontant.dart';
import 'package:jrendement/components/sectionText.dart';
import 'package:jrendement/entities/projet.dart';
import 'package:jrendement/helpers/routes.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:provider/provider.dart';

/**
 * Projet
 * Hocine Triki (2021)
 */
class Projects extends StatelessWidget {

  Projet _projet;

  Projects (this._projet);

  @override
  Widget build(BuildContext context) {
    return WillPopScope (
      onWillPop: () async => false,
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: ChangeNotifierProvider (
            create: (_) => ValueProvider(this._projet),
            child: MyHomePage(),
          )
      ),
    );
  }
}

class MyHomePage extends  StatelessWidget {


@override
Widget build(BuildContext context) {

  final _vP = Provider.of<ValueProvider>(context);

  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      // No appbar provided to the Scaffold, only a body with a
      drawer: MyDrawer(),
      // CustomScrollView.
      body: CustomScrollView(
        slivers: <Widget>[
          MySliverAppBar(),

          MySection(SectionAchat()),
          MySection(SectionLoyer()),

          MySection(SectionText([
            Text('Le rendement brut ne prend pas en compte la situation personnelle de l\'acheteur ou encore les charges à payer.'),
            Text('Il s’agit des gains des loyers sur une année rapportés au coût d’achat du bien immobilier.'),
            Text('C\'est donc un calcul de base qui donne une première idée du rendement de l\'investissement')
          ])),

          // Affichage du brut
          MySection(SectionMontant(
              "Rendement brut",
              _vP.projet.brut.toStringAsFixed(2),
              "Calcul du rendement brut",
              "%",
              [
                Text('Le rendement brut = loyerAnnuel * 100 / achat'),
                Text('Sachant que le loyer annuel tient compte du nombre de jours où le bien a été vaccant'),
                Text('Le rendement brut ne tient pas compte des frais d\'acquisition'),
              ]
          )),

          MySection(SectionFraisAchat()),
          MySection(SectionFinancement()),
          MySection(SectionFraisAnnee()),
          MySection(SectionText([
            Text('Le rendement net permet de calculer un rendement plus proche de la réalité. '
                  'Il s’agit de tenir compte des différents frais liés à la possession, à l\'entretien ou à la gestion du bien, etc.'),
            Text('C\'est le calcul idéal pour savoir si votre investissement locatif est intéressant ou non.'),
          ])),

          MySection(SectionMontant(
              "rendement net de charges",
              _vP.projet.net.toStringAsFixed(2),
              "Calcul du rendement net",
              "%",
              [
                Text("Le rendement net = Total recette * 100 / Total cout acquisition"),
                Text("Total recette = (loyer annuel - frais) / 100"),
                Text("Total acquisition = achat + frais d'acquisition + cout du crédit"),
              ]
              //this._net = (loyerAnnuel - fraisAnnee) * 100 / (achat + fraisAcquisition + _totalCreditToutCompris);

          )),
          MySection(SectionDefiscalisation()),
          MySection(SectionMontant(
            "Rendement net net ",
            _vP.projet.netNet.toStringAsFixed(2),
            "Rendement net net",
            "%",
            [
              Text("Le calcul du rendement net net est le même que celui du rendement net !\n"
                   "Auquel il faut retirer la somme dûe aux impôts ou ajouter le montant de la défiscalisation."
              ),
            ],
          )),
          MySection(SectionText([
            Text("Le cash flow ne tient pas compte des couts d'acquisition !", style: TextStyle(fontWeight: FontWeight.bold),),
            Text("Il s’agit juste de ce que vous allez gagner (ou perdre) dans l'année.\n"
                 "Autrement dit cela correspond au loyer annuel (+ défiscalisation) moins les charges, le remboursement du crédit et les impôts"),
          ])),
          MySection(SectionMontant(
              "Cash flow (annuel) ",
              _vP.projet.cash.toStringAsFixed(2),
              "Calcul du cash flow",
              "€",
              [
                Text("Cash flow = Total des recettes - Total des charges - le remboursement du crédit\n"
                     "Total recettes = Somme des loyers + montant de la défiscalisation\n"
                     "Total charges = Somme des dépenses durant l'année (avec les impôts)\n"
                     "Crédit = Somme des mensualités liès au crédit\n"
                     "Les impôts ne sont pas pris en compte"
                ),

              ],
          )),
          MySection(SectionBoutton())
        ],
      ),
      bottomNavigationBar: MyBottomBar(),
      floatingActionButton: MyFloatingActionButton(context, Routes.main(), "Page Principale", Icons.home),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    ),
  );
  }
}