import 'package:flutter/material.dart';
import 'package:jrendement/components/myBottomBar.dart';
import 'package:jrendement/components/myDrawer.dart';
import 'package:jrendement/components/myFloatingActionButton.dart';
import 'package:jrendement/helpers/routes.dart';


/**
 * A propos de ....
 * (Hocine Triki 2021)
 */
class About extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      drawer: MyDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Hocine Triki',
              style: TextStyle(color: Colors.blue, fontSize: 20),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: Text('2021'),
            )
          ],
        ),
      ),
      bottomNavigationBar: MyBottomBar(),
      floatingActionButton: MyFloatingActionButton(context, Routes.main(), "Page Principale", Icons.home),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }

}
