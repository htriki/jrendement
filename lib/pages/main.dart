
import 'package:flutter/material.dart';
import 'package:jrendement/components/myBottomBar.dart';
import 'package:jrendement/components/myDrawer.dart';
import 'package:jrendement/components/myFloatingActionButton.dart';
import 'package:jrendement/components/mySection.dart';
import 'package:jrendement/components/mySliverAppBar.dart';
import 'package:jrendement/entities/projet.dart';
import 'package:jrendement/helpers/routes.dart';

/**
 * Hocine Triki * 2021
 * Page d'accueil
 */
class Main extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp (
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Projet> listProjet = [];

  @override
  void initState() {
    super.initState();
    _findAll();
  }

  _findAll() {
    Projet.findAll().then((l) {
      this.setState(() {
          this.listProjet = l;
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      bottomNavigationBar: MyBottomBar(),
      drawer: MyDrawer(),
      body: CustomScrollView(
        slivers: <Widget>[
          MySliverAppBar(),

          (listProjet != null && listProjet.length > 0)
          ?
            _getSlivers(listProjet, context)
          :
            MySection(Info())
        ]
      ),

      floatingActionButton: MyFloatingActionButton(context, Routes.newProjects(), "Nouveau Projet", Icons.add),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}

SliverList _getSlivers(List myList, BuildContext context) {
  return SliverList(
    delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
        return buildRow(myList[index], context);
      },
      childCount: myList.length,
    ),
  );
}

buildRow(Projet projet, BuildContext context) {
  return
    InkWell (
      onTap: ()=> Navigator.of(context).push(Routes.projects(projet)),
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(width: 1, color: Colors.brown),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: Text(projet.nom,
                  style: TextStyle(color: Colors.indigo, fontWeight: FontWeight.bold, fontSize: 16)),
            ),
            Text("Prix d'achat : ${projet.achat} €"),
            Text("Loyer mensuel : ${projet.loyerMensuel} €"),
            MyRichText("${projet.brut.toStringAsFixed(2)} %", "${projet.net.toStringAsFixed(2)} %"),

          ],
        ),
      ),
    );
}

class Info extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return
      Container(
          width: double.infinity,
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.all(50),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            border: Border.all(width: 1, color: Colors.brown),
          ),
          child: Column(
            children: [
              Text("JRendement vous permet de simuler de manière très précise la rentablité d'un investissement locatif."
                   "\n\n"
                   "Visualisez très rapidement la rendement brut et net de votre investissement !"
              ),
              Center (
                child: Text("\n\n"
                     "Cliquez sur"),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: CircleAvatar(
                    backgroundColor: Colors.teal,
                    child: Text("+",
                      style: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold),)
                ),
              ),
              Center(
                child:
                  Text("pour créer une nouvelle simulation"),
              ),
            ],
          )
      );
  }

}



/*class ListTuile extends StatelessWidget {

  List<Projet> _listProjet;

  ListTuile(this._listProjet);

  @override
  Widget build(BuildContext context) {

    return new ListView.builder (
      itemCount: _listProjet.length,
      itemBuilder: (BuildContext ctxt, int index) {

        return ListTile( leading: Icon(Icons.account_balance_outlined, color: Colors.blue,),
          title: Text('Mon projet (${this._listProjet[index].id})', style: TextStyle(fontWeight: FontWeight.bold),),
          subtitle: Padding(
            padding: const EdgeInsets.only(left: 20, top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start ,
              children: [
                MyRichText("Prix du bien : ", "128 000 €"),
                MyRichText("Loyer : ", "560 €"),
                Text("Rendement brut : 5.25 %"),
                Text("Rendement net : 3.25 %"),
              ],
            ),
          ),
          trailing: Icon(Icons.ac_unit),
          onTap: ()=> Navigator.of(context).push(Routes.projects(this._listProjet[index].id))
        );
      }
    );
  }
}*/

class MyRichText extends StatelessWidget {

  final String _brut;
  final String _net;

  MyRichText(this._brut, this._net);

  @override
  Widget build(BuildContext context) {
    return
      RichText(
        text: TextSpan(
          text: "Rendement brut : ",
          style: TextStyle(color: Colors.black),
          children: <TextSpan>[
            TextSpan(
              text: this._brut,
              style: TextStyle (color: Colors.black, fontWeight: FontWeight.bold,),
            ),
            TextSpan(
              text: ", net : ",
            ),
            TextSpan(
              text: this._net,
              style: TextStyle (color: Colors.black, fontWeight: FontWeight.bold,),
            )
          ],
        ),
      );
  }

}
