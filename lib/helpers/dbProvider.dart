
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final int VERSION = 4;
final String DATABASENAME = 'Jrendement.db';

/**
 * Hocine Triki * 2021
 * Accés à la base de données
 */
class DbProvider {

  DbProvider._privateConstructor();
  static final DbProvider instance = DbProvider._privateConstructor();

  Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return this._db;
    } else {
      this._db = await this._initDatabase();
      return this._db;
    }
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory(); // path de l'application
    String path = join(documentsDirectory.path, DATABASENAME); // path contient le chemin de l'appli + la bdd
    print(path);
    return await openDatabase(path, version: VERSION, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    if (oldVersion < newVersion) {
      db.execute("DROP TABLE IF EXISTS projet;");
      db.execute(_getSqlCreateProjet);
    }
  }

  Future close() async => _db.close();

  Future _onCreate(Database db, int version) async {
    await db.execute(_getSqlCreateProjet);
  }

  String _getSqlCreateProjet =  '''  
      create table projet (
        id integer primary key autoincrement,
        nom varchar(100) not null,        
        achat real,
        loyerMensuel real,
        vacances real,        
        notaire real, 
        travaux real,
        meuble real,
        fraisAgence real,
        courtier real,
        autreFraisAchat real,        
        taxeFonciere real,
        assurance real,
        assuranceImpaye real,
        gestionLocative real,
        copropriete real, 
        entretien real,
        autresCharges real,       
        montantFi real,
        tauxFi real,
        dureeFi real,
        tauxAssuranceFi real, 
        mensualiteFi real,
        montantTotalInteretFi real,
        montantAssuranceMensuelFi real,
        montantAssuranceTotalFi real,
        mensualiteFiToutCompris real,
        annuiteFiToutCompris real,
        totalCreditToutCompris real,        
        brut real,
        net real,
        netNet real,
        cash,        
        typeImpot integer,
        impotAnnuel real,
        defiscalisation real
      )
   ''';
}