import 'package:flutter/material.dart';
import 'package:jrendement/entities/projet.dart';
import 'package:jrendement/pages/about.dart';
import 'package:jrendement/pages/main.dart';
import 'package:jrendement/pages/projects.dart';

class Routes {

  static Route createRoute(r) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => r,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {

        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Route main() {
    return Routes.createRoute(Main());
  }

  static Route about() {
    return Routes.createRoute(About());
  }

  static Route projects(Projet projet) {
    return Routes.createRoute(Projects(projet));
  }

  static Route newProjects() {
    return Routes.createRoute(Projects(new Projet()));
  }
}
