import 'package:flutter/material.dart';
import 'package:jrendement/components/myInformationButton.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:provider/provider.dart';

/// Hocine Triki * 2021
class SectionMontant extends StatelessWidget {

  String _label = "";
  String _montant = "0";
  String _title;
  String _unite;
  List<Text> _list;

  SectionMontant(this._label, this._montant, this._title, this._unite, this._list);

  final TextStyle ts = TextStyle(
      color: Colors.brown,
      fontSize: 16,
      fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.lightBlueAccent,
            Colors.white,
          ],
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 6,
                child: Text(
                    '${_label} : ${_montant} ${this._unite}', style: ts
                )
              ),
              Expanded(
                flex: 1,
                child: CircleAvatar(
                  radius: 14,
                  backgroundColor: Colors.blue,
                  child: IconButton(
                    icon: Icon(Icons.refresh, size: 14, color: Colors.white),
                    onPressed: ()=> {
                      _vP.calculateAndNotify()
                    },
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: MyInformationButton(this._title, this._list),
              )
            ],
          )
        ],
      ),
    );
  }
}
