

import 'package:flutter/material.dart';

/**
 * Hocine Triki * 2021
 * Class d'affichage d'une popup
 */
class MyPopup {

  static TextStyle textStyle = new TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.blue);

  static show(context, String title, List<Text> list) {

    showDialog(context: context,
        builder:
            (BuildContext context) {
          return AlertDialog(
            title: Text(title, style: textStyle,),
            shape: OutlineInputBorder(borderRadius: BorderRadius.circular(18.0)),
            content: SingleChildScrollView(
              child: ListBody(
                children: list,
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Fermer'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
    );
  }
}