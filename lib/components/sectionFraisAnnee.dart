import 'package:flutter/material.dart';
import 'package:jrendement/components/myText.dart';
import 'package:jrendement/components/zone.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/constants.dart' as C;
import 'package:jrendement/types/typeZone.dart';
import 'package:provider/provider.dart';

/**
 * Hocine Triki * 2021
 */
class SectionFraisAnnee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
/*
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.orange,
            Colors.white,
          ],
        ),
*/
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(180, 'Charges sur l\'année',
              [
                Text("Reportez les charges courantes supportées durant l'année."),
                Text("Les charges de copropriété, la taxe foncière annuelle, les éventuelles charges afférentes à "
                     "la gestion de la location par une agence et / ou les charges liées à des travaux, ...\netc."),
              ]),
          Zone("Taxe foncière",     _vP.get(C.TAXE_FONCIERE), 100, 0, 0, "€", C.TAXE_FONCIERE, _vP, TypeZone.integer),
          Zone("Assurance",         _vP.get(C.ASSURANCE), 100, 0, 0, "€", C.ASSURANCE, _vP, TypeZone.integer),
          Zone("Ass. impayé",       _vP.get(C.ASSURANCE_IMPAYE), 100, 0, 0, "€", C.ASSURANCE_IMPAYE, _vP, TypeZone.integer),
          Zone("Gestion locative",  _vP.get(C.GESTION_LOCATIVE), 100, 0, 0, "€", C.GESTION_LOCATIVE, _vP, TypeZone.integer),
          Zone("Copropriété",       _vP.get(C.COPROPRIETE), 100, 0, 0, "€", C.COPROPRIETE, _vP, TypeZone.integer),
          Zone("Entretien",         _vP.get(C.ENTRETIEN), 100, 0, 0, "€", C.ENTRETIEN, _vP, TypeZone.integer),
          Zone("Autres charges",    _vP.get(C.AUTRES_CHARGES), 100, 0, 0, "€", C.AUTRES_CHARGES, _vP, TypeZone.integer),

        ],
      ),
    );
  }
}
