import 'package:flutter/material.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:provider/provider.dart';

class SectionNotaire extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.grey,
            Colors.white,
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Frais de notaire'),
          Row(
            children: <Widget>[
/*              Expanded(
                  flex: 1,
                  child:
                  new Radio(
                    value: false,
                    groupValue: _vP.cancelNotaire,
                  )
              ),*/
              Expanded(
                  flex: 2,
                  child:
                    Text("Faux")

              )
            ],
          ),
          Row(
            children: <Widget>[
/*              Expanded(
                  flex: 1,
                  child:
                  new Radio(
                    value: true,
                    groupValue: _vP.cancelNotaire,
                    //onChanged: _vP.voidTrue()
                  )

              ),*/
              Expanded(
                  flex: 2,
                  child:
                  Text("Vrai")

              )
            ],
          ),
          Text(
            'notaire 1',
            style: TextStyle(
              color: Colors.brown,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'notaire 2',
            style: TextStyle(
              color: Colors.brown,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
