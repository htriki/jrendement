import 'package:flutter/material.dart';

/**
 * A propos de ....
 * (Hocine Triki 2021)
 */
class MyFloatingActionButton extends FloatingActionButton {

  MyFloatingActionButton(context, route, tooltip, icon):super(
    onPressed: ()=> Navigator.of(context).push(route),
    tooltip: tooltip,
    backgroundColor: Colors.teal,
    child: Icon(icon),
  );
}