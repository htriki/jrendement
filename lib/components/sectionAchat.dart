import 'package:flutter/material.dart';
import 'package:jrendement/components/zone.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/constants.dart' as Constants;
import 'package:jrendement/types/typeZone.dart';
import 'package:provider/provider.dart';

/**
 * Hocine Triki * 2021
 * Section achat du bien
 */
class SectionAchat extends StatelessWidget {

  static TextStyle textStyle = new TextStyle(fontWeight: FontWeight.bold, fontSize: 12, color: Colors.brown);

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
        /*gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.greenAccent,
            Colors.white,
          ],
        ),*/
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Achat',
            style: TextStyle(
              color: Colors.brown,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 10),
            child: Container(
              child: Row(
                children: [
                  Expanded (
                    flex: 1,
                    child: Text('Nom du projet : ', style: textStyle,),
                  ),
                  Expanded(
                    flex: 3,
                    child: TextField(
                      maxLength: 100,
                      controller: TextEditingController(text: _vP.projet.nom),
                      onChanged: (text) {
                        _vP.projet.nom = text;
                      },
                    ),
                  ),
                ]
              ),
            ),
          ),
          Zone("Prix du bien", _vP.get(Constants.ACHAT), 10000, 0, 0, "€", Constants.ACHAT, _vP, TypeZone.integer),
        ],
      ),
    );
  }
}