import 'package:flutter/material.dart';
import 'package:jrendement/components/myText.dart';
import 'package:jrendement/components/myInformationButton.dart';
import 'package:jrendement/components/myPopup.dart';
import 'package:jrendement/components/zone.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/typeZone.dart';
import 'package:provider/provider.dart';
import 'package:jrendement/types/constants.dart' as Constants;

/**
 * Hocine Triki * 2021
 * Section Loyer
 */
class SectionLoyer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(150, 'Loyer',
              [
                Text('Le loyer est à indiquer hors charges !!!'),
                Text('Les vacances locatives correspondent au nombre de jours dans l\'année où le bien ne sera pas loué'),
              ]),
          Zone("Loyer Mensuel", _vP.get(Constants.LOYER_MENSUEL), 25, 0, 0, "€", Constants.LOYER_MENSUEL, _vP, TypeZone.integer),
          Zone("Vacances locatives", _vP.get(Constants.VACANCES), 1, 0, 0, "j", Constants.VACANCES, _vP, TypeZone.integer),
        ],
      ),
    );
  }
}