

import 'package:flutter/material.dart';

/**
 * Hocine Triki * 2021
 */
class MyText extends StatefulWidget {

  double _endPosition = 0;
  List<Text> _list;
  String _title;

  MyText(this._endPosition, this._title, this._list);
  _MyTextState createState() => _MyTextState();
}

class _MyTextState extends State<MyText> {

  static TextStyle ts = TextStyle(color: Colors.brown, fontSize: 18, fontWeight: FontWeight.bold);
  static Icon iDown = Icon(Icons.arrow_circle_down_rounded, size: 22, color: Colors.indigo);
  static Icon iUp = Icon(Icons.arrow_circle_up_rounded, size: 22, color: Colors.red);

  double height = 0;
  Icon iconSelected = iDown;

  _push() {
    setState(() {
      if (height == 0) {
        height = widget._endPosition;
        iconSelected = iUp;
      } else {
        height = 0;
        iconSelected = iDown;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextButton(
            child: Text(
              widget._title,
              style: ts,
            ),
            onPressed: () {this._push();}),
          IconButton(icon: iconSelected,
            onPressed: ()=> {
                this._push()
            },
          ),
          //MyInformationButton('Pour information !', widget._list),
        ],
      ),
      AnimatedContainer(
        duration: new Duration(milliseconds: 500),
        height: height,
        child: SingleChildScrollView(
          child: Container(

            width: double.infinity,
            padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(width: 1, color: Colors.brown),
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.blueGrey,
                  Colors.white,
                ],
              ),

            ),

            child: Column (
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget._list
            ),
          ),
        )
      ),
    ]);
  }
}