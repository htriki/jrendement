import 'package:flutter/material.dart';
import 'package:jrendement/helpers/routes.dart';

/**
 * Hocine Triki * 2021
 *
 */
class MyDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                image: new DecorationImage(
                  image: new ExactAssetImage('assets/images/ciel.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              accountName: Text("JRendement", style: TextStyle(color: Colors.black),),
              accountEmail: Text("hocine.triki@gmail.com", style: TextStyle(color: Colors.black)),
/*
              currentAccountPicture: GestureDetector(
                child: CircleAvatar(
                  child: Text(
                    "AM",
                    style: TextStyle(
                        color: Colors.pink,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                  backgroundColor: Colors.white,
                ),
              ),
*/
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                onTap: () {
                  Navigator.of(context).push(Routes.main());
                },
                leading: Icon(Icons.home, color: Colors.pink),
                title: Text("Page d'accueil"),
              ),
            ),
            InkWell(
              onTap: null,
              child: ListTile(
                onTap: () {
                  Navigator.of(context).push(Routes.newProjects());
                },
                leading: Icon(Icons.person, color: Colors.teal),
                title: Text("Nouveau projet"),
              ),
            ),
            Divider(color: Colors.blue),
            InkWell(
              onTap: null,
              child: ListTile(
                leading: Icon(Icons.help, color: Colors.blue),
                title: Text("A propos"),
                onTap: () {
                  Navigator.of(context).push(Routes.about());
                },
              ),
            ),
/*            InkWell(
              onTap: null,
              child: ListTile(
                leading: Icon(
                  Icons.power_settings_new,
                  color: Colors.black,
                ),
                title: Text("Log out"),
              ),
            )*/
          ],
        ));
  }
}
