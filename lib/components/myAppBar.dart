import 'package:flutter/material.dart';

/**
 * A propos de ....
 * (Hocine Triki 2021)
 */
class MyAppBar extends AppBar {
  MyAppBar():super(
    title: Text('Rendement Locatif', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
    flexibleSpace: Image(
      image: AssetImage('assets/images/ciel.jpg'),
      fit: BoxFit.cover,

    ),
    backgroundColor: Colors.transparent, //elevation: 2000.0,
    toolbarHeight: 80
  );
}