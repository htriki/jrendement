import 'package:flutter/material.dart';
import 'package:jrendement/components/myPopup.dart';

/// Hocine Triki
/// Bouton d'information
class MyInformationButton extends StatelessWidget {

  String _title;
  List<Text> _list;

  /// Titre de la popup et liste de lignes à afficher !
  MyInformationButton(this._title, this._list);

  @override
  Widget build(BuildContext context) {

    return InkWell(
      onTap: ()=>{
        MyPopup.show(context, this._title, this._list )
      },
      child: Container(
        width: 25.0,
        height: 25.0,
        decoration: new BoxDecoration(
          color: Colors.red, // border color
          shape: BoxShape.circle,
          border: Border.all(color: Colors.lightBlueAccent)
        ),
        child: CircleAvatar(
            backgroundColor: Color.fromRGBO(1, 32, 161, 1),
            child: Text("i",
              style: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold),)
        ),
      ),
    );
  }

}