

import 'package:flutter/material.dart';
import 'package:jrendement/components/mySection.dart';
import 'package:jrendement/entities/projet.dart';

class MyProject extends StatelessWidget {

  Projet _projet;

  MyProject(this._projet);

  @override
  Widget build(BuildContext context) {

    return Container (

      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
      ),
      child: Text(">>>>  id : ${_projet.id}"),
    );
  }

}