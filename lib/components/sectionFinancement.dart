import 'package:flutter/material.dart';
import 'package:jrendement/components/myText.dart';
import 'package:jrendement/components/zone.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/constants.dart' as C;
import 'package:jrendement/types/typeZone.dart';
import 'package:provider/provider.dart';

/**
 * Hocine Triki * 2021
 * Financement du bien
 */
class SectionFinancement extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);
    Widget widgetShowFi = (_vP.projet.mensualiteFi == 0) ?
      Text("") :
      ShowValueFi(_vP.projet.mensualiteFi,
                  _vP.projet.montantTotalInteretFi,
                  _vP.projet.montantAssuranceMensuelFi,
                  _vP.projet.montantAssuranceTotalFi,
                  _vP.projet.mensualiteFiToutCompris,
                  _vP.projet.annuiteFiToutCompris,
                  _vP.projet.totalCreditToutCompris,
                  _vP.get(C.FI_DUREE));

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(150, 'Financement',
              [
                Text("Indiquez si vous passez par un emprunt bancaire pour financer l'achat de votre bien."),
                Text("L'emprunt entre dans le calcul du taux de rentabilité net."),
                Text("Quant au coût du crédit annuel il est pris en compte dans le calcul du cash flow.")
              ]),
          Zone("Montant", _vP.get(C.FI_MONTANT), 50000, 0, 0, "€", C.FI_MONTANT, _vP, TypeZone.integer),
          Zone("Taux", _vP.get(C.FI_TAUX), 0.5, 0, 0, "%", C.FI_TAUX, _vP, TypeZone.decimal),
          Zone("Durée (année)", _vP.get(C.FI_DUREE), 5, 0, 0, "an", C.FI_DUREE, _vP, TypeZone.integer),
          Zone("Taux assurance", _vP.get(C.FI_TAUX_ASS), 0.5, 0, 0, "%", C.FI_TAUX_ASS, _vP, TypeZone.decimal),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align (
              alignment: Alignment.center,
              child: CircleAvatar(
                radius: 14,
                backgroundColor: Color.fromRGBO(1, 32, 161, 1),
                child: IconButton(
                  icon: Icon(Icons.refresh, size: 14, color: Colors.white),
                  onPressed: ()=> {_vP.calculateAndNotify()},
                ),
              ),
            ),
          ),
          widgetShowFi

        ],
      ),
    );
  }
}

class ShowValueFi extends StatelessWidget {

  static TextStyle textStyle1 = new TextStyle(fontWeight: FontWeight.normal, fontSize: 13, color: Colors.brown);
  static TextStyle textStyle2 = new TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.brown);

  double _mensualiteFi = 0;
  double _montantTotalInteretFi = 0;
  double _montantAssuranceMensuelFi = 0;
  double _montantAssuranceTotalFi = 0;
  double _mensualiteFiToutCompris = 0;
  double _annuiteFiToutCompris = 0;
  double _totalCreditToutCompris = 0;
  double _an = 0;

  ShowValueFi(this._mensualiteFi, this._montantTotalInteretFi, this._montantAssuranceMensuelFi, this._montantAssuranceTotalFi,
              this._mensualiteFiToutCompris, this._annuiteFiToutCompris, this._totalCreditToutCompris, this._an);

  @override
  Widget build(BuildContext context) {

    String annee = (this._an > 1) ? "ans" : "an";

    return Container(
        padding: EdgeInsets.only(bottom: 10, top:20, left: 10, right: 10),
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text("Mensualité (hors assurance) :  ${this._mensualiteFi.toStringAsFixed(2)} €", style: textStyle1,)
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Text("Total des intêrets (cout du crédit hors assurance) :  ${this._montantTotalInteretFi.toStringAsFixed(2)} €", style: textStyle1,))
              ],
            ),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Assurance / mois  :  ${this._montantAssuranceMensuelFi.toStringAsFixed(2)} €", style: textStyle1,),
                    ))
              ],
            ),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Text("Assurance total au bout de ${this._an} ${annee} : ${this._montantAssuranceTotalFi.toStringAsFixed(2)} €", style: textStyle1,))
              ],
            ),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Mensualité assurance comprise : ${this._mensualiteFiToutCompris.toStringAsFixed(2)} €", style: textStyle2,),
                    ))
              ],
            ),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Text("Annuité assurance comprise : ${this._annuiteFiToutCompris.toStringAsFixed(2)} €", style: textStyle1,),
                    )
              ],
            ),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Coût total du crédit (ass. comprise) au bout de ${this._an} ${annee} :  ${this._totalCreditToutCompris.toStringAsFixed(2)} €", style: textStyle2,),
                    ))
              ],
            )
          ],
        )
    );
  }

}