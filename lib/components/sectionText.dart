import 'package:flutter/material.dart';

/**
 * hocine triki * 2021
 */
class SectionText extends StatelessWidget {

  List<Text> _list;

  SectionText(this._list);

  @override
  Widget build(BuildContext context) {

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: this._list,
      ),
    );
  }
}