import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/typeZone.dart';

/**
 * Hocine Triki * 2021
 */
class Zone extends StatelessWidget {

  static TextStyle textStyle = new TextStyle(fontWeight: FontWeight.bold, fontSize: 12, color: Colors.brown);
  static TextStyle textStyleDisabled = new TextStyle(fontWeight: FontWeight.bold, fontSize: 12, color: Colors.grey);

  String _label;
  double _value = 0;
  double _augmentation = 0;
  double _min = 0;
  double _max = 0;
  String _symbol = "";
  String _key = "";
  ValueProvider _vP;
  TypeZone _typeZone;
  int _nombreDecimal = 0;
  FilteringTextInputFormatter _filterNumber;
  bool _enabled = true;
  TextStyle _ts = null;

  Zone(this._label, this._value, this._augmentation, this._min, this._max, this._symbol, this._key, this._vP,
       this._typeZone, [this._enabled = true]) {
    if (TypeZone.decimal == this._typeZone) {
      this._nombreDecimal = 2;
      _filterNumber = FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d*'));
    } else {
      _filterNumber = FilteringTextInputFormatter.digitsOnly;
    }

    if (this._enabled) {
      this._ts = textStyle;
    } else {
      this._ts = textStyleDisabled;
    }
  }

  @override
  Widget build(BuildContext context) {

    /* gestion de la position du cursor
    var c = new TextEditingController.fromValue(
        new TextEditingValue(text: this._value.toStringAsFixed(this._nombreDecimal),
            selection: new TextSelection.collapsed(offset: this._vP.position)));
    */

    return Container(
      padding: EdgeInsets.all(5),
      height: 55,
      color: Colors.transparent,
      child: Row (
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Visibility (
              visible: this._enabled,
              child: CircleAvatar(
                radius: 14,
                backgroundColor: Colors.black26,
                child: IconButton(
                  onPressed: ()=> {
                    if (this._vP.get(_key) > this._min) {
                      this._vP.addAndNotify(_key, -this._augmentation),
                      if (this._vP.get(_key) < this._min) {
                        this._vP.modifyAndNotify(_key, this._min)
                      }
                    }
                  },
                  icon: Icon(Icons.remove, size: 12, color: Colors.black,),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 7,
            child: Padding(
              padding: const EdgeInsets.only(left: 5, right: 5, top: 1, bottom: 1),
              child: Container (
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(color: Colors.greenAccent, spreadRadius: 1),
                  ],
                ),

                child: Row(
                  children: <Widget> [
                    Expanded(
                        flex: 6,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                                padding: EdgeInsets.only(right: 5),
                                child: Text(this._label, style: this._ts)
                            )
                          ),
                        ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child: TextField (
                            enabled: _enabled,
                            //autofocus: true,
                            onChanged: (text) {
                              //_vP.position = c.selection.baseOffset;
                              double d = 0;
                              if (text != '') {
                                d = (num.parse(text)).toDouble();
                              }
                              this._vP.modify(_key, d);
                            },
                            //controller: c,
                            controller: TextEditingController(text: this._value.toStringAsFixed(this._nombreDecimal)),
                            textAlign: TextAlign.end,
                            cursorWidth: 2,
                            keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                _filterNumber
                                //FilteringTextInputFormatter.digitsOnly,
                                //FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d*')),
                                //FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                            decoration: new InputDecoration(
                              border: InputBorder.none
                            ),
                            style: this._ts
                          ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(this._symbol, style: this._ts,)
                    )
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Visibility (
              visible: this._enabled,
              child: CircleAvatar(
                radius: 14,
                backgroundColor: Colors.black26,
                child: IconButton(
                  onPressed: ()=>{
                    if (this._max == 0) {
                      this._vP.addAndNotify(_key, _augmentation)
                    } else {
                      if (this._vP.get(_key) < this._max) {
                        this._vP.addAndNotify(_key, _augmentation)
                      }
                    }
                  },
                  icon: Icon(Icons.add, size: 12, color: Colors.black ),
                ),
              ),
            ),
          ),

        ],
      ),

    );
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final regEx = RegExp(r"^\d*\.?\d*");
    String newString = regEx.stringMatch(newValue.text) ?? "";
    return newString == newValue.text ? newValue : oldValue;
  }
}