
import 'package:flutter/material.dart';

/**
 * Hocine Triki * 2021
 * Définition d'une section
 */
class MySection extends SliverList {

    MySection(Widget widget):super(
      delegate: SliverChildBuilderDelegate(
            (context, index) {
          return widget;
        },
        childCount: 1,
      ),
    );
}

