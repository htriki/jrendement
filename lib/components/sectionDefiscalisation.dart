import 'package:flutter/material.dart';
import 'package:jrendement/components/myText.dart';
import 'package:jrendement/components/myInformationButton.dart';
import 'package:jrendement/components/zone.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/constants.dart' as C;
import 'package:jrendement/types/typeZone.dart';
import 'package:provider/provider.dart';

/**
 * Hocine Triki * 2021
 *
 */
class SectionDefiscalisation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    bool isImpot = _vP.projet.isImpot();

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(350, 'Impôt / Défiscalisation',
              [
                Text('C\'est la rentabilité nette de charges, plus les éventuels avantages fiscaux\n'),
                Text('Impôts', style: TextStyle(fontWeight: FontWeight.bold),),
                Text('Indiquez ici ce que vous allez payer en plus avec l\'achat de ce bien.'),
                Text(''),
                Text('Défiscalisation', style: TextStyle(fontWeight: FontWeight.bold)),
                Text("C'est l'ensemble de mesures mises en place pour diminuer légalement l'impôt sur le revenu.\n"
                     "En général lorsque vous achetez un bien neuf, dans certain cas il est possible de bénéficier d'un abattement d'impôt, (exemple Pinel, ou le dispositif Censi-Bouvard dans l'immobilier neuf, ou encore les avantages fiscaux Cosse, Denormandie dans l'ancien...\n"
                     "Indiquer ici le montant déduit (gagné) sur l'année'"),
              ]),
          Column(
            children : <Widget>[
              Row (
                children: [
                  Expanded(flex: 1,
                      child: Radio(
                          value: 1,
                          groupValue: _vP.projet.typeImpot,
                          onChanged: (int value) => _vP.typeImpot = value,
                        ),
                      ),
                  Expanded(flex: 10,
                      child: Zone("Impôt annuel", _vP.get(C.IMPOT_ANNUEL), 100, 0, 0, "€", C.IMPOT_ANNUEL, _vP, TypeZone.integer, isImpot),
                  )
                ],
              ),
              Row (
                children: [
                  Expanded(flex: 1,
                    child: Radio(
                        value: 2,
                        groupValue: _vP.projet.typeImpot,
                        onChanged: (int value) => _vP.typeImpot = value,
                      ),
                    ),
                  Expanded(flex: 10,
                    child: Zone("Gain impôt", _vP.get(C.DEFISCALISATION), 100, 0, 0, "€", C.DEFISCALISATION, _vP, TypeZone.integer, !isImpot),
                  )
                ],
              ),


            ],
          ),

        ],
      ),
    );
  }
}
