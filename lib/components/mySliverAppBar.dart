
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

/**
 * Hocine Triki * 2021
 */
class MySliverAppBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return  SliverAppBar (
      expandedHeight: 100.0,
      pinned: true, // <-- Setting this to true make the header not sticky on scroll
      floating: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text("Rendement Locatif", style: TextStyle(color: Colors.black87, fontSize: 16.0,)),
        background: Image(image: new AssetImage('assets/images/ciel.jpg'), fit: BoxFit.cover,),
      ),
    );
  }

}