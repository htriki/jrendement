import 'package:flutter/material.dart';
import 'package:jrendement/entities/projet.dart';
import 'package:jrendement/helpers/routes.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:provider/provider.dart';

/// Hocine Triki * 2021
class SectionBoutton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          Visibility(
            visible: _vP.isCreation(),
            child: Center (
              child: ButtonSave(),
            )
          ),
          Visibility(
            visible: !_vP.isCreation(),
            child: Row(
              children: [
                Expanded(
                    flex: 3,
                    child: ButtonSave()
                ),
                Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: ElevatedButton.icon(
                        label: Text('Suppr'),

                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(15),
                          primary: Colors.white,
                          onPrimary: Colors.red,
                          //onSurface: Colors.grey,
                        ),
                        icon: Icon(Icons.delete_forever_outlined),
                        onPressed: () {
                          _showMyDialog(context, _vP.projet);
                        },
                      ),
                    )
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> _showMyDialog(context, projet) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Suppression', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.blue)),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text('Confirmez la suppression du projet'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Projet.delete(projet.id);
                Navigator.of(context).push(Routes.main());
              },
            ),
            TextButton(
              child: Text('Annuler'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

}

Future<void> _showMyAlert(context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Information', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.blue)),
        content: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Les champs suivants sont obligatoires : "),
              Text("Le nom du projet"),
              Text("Le montant de l'achat"),
              Text("Le montant du loyer"),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Annuler'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],

      );
    },
  );
}

class ButtonSave extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return ElevatedButton.icon(
      label: Text('Sauver'),
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(15),
        primary: Colors.white,
        onPrimary: Colors.blue,
      ),
      icon: Icon(Icons.web),
      onPressed: () {

        if (_vP.isOkBeforeSave()) {
          _vP.save();
          final SnackBar _snack = SnackBar(
            content: Text("Le projet a été enregistré !"),
            duration: Duration(seconds: 3),
            backgroundColor: Colors.black26,
          );
          Scaffold.of(context).showSnackBar(_snack);
        } else {
          _showMyAlert(context);
        }
      },
    );
  }

}