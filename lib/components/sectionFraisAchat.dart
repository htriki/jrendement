import 'package:flutter/material.dart';
import 'package:jrendement/components/myText.dart';
import 'package:jrendement/components/myInformationButton.dart';
import 'package:jrendement/components/zone.dart';
import 'package:jrendement/providers/valueProvider.dart';
import 'package:jrendement/types/constants.dart' as C;
import 'package:jrendement/types/typeZone.dart';
import 'package:provider/provider.dart';

/**
 * Hocine Triki * 2021
 * Cout d'acquisition
 */
class SectionFraisAchat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final _vP = Provider.of<ValueProvider>(context);

    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(bottom: 20, top:20, left: 10, right: 10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(width: 1, color: Colors.brown),
/*
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.orange,
            Colors.white,
          ],
        ),
*/
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(200, 'Frais d\'acquisition',
              [
                Text('Les frais d\'acquisition correspondent aux sommes déboursées au moment de l\'achat du bien : '),
                Text('- Les frais de notaire'),
                Text('- Les travaux engagés pour renover le bien immobilier'),
                Text('- Les frais d\'agence'),
                Text('- etc.')
              ]),
          Zone("Notaire", _vP.get(C.NOTAIRE), 100, 0, 0, "€", C.NOTAIRE, _vP, TypeZone.integer),
          Zone("Travaux", _vP.get(C.TRAVAUX), 100, 0, 0, "€", C.TRAVAUX, _vP, TypeZone.integer),
          Zone("Meuble", _vP.get(C.MEUBLE), 100, 0, 0, "€", C.MEUBLE, _vP, TypeZone.integer),
          Zone("Frais d'agence", _vP.get(C.FRAIS_AGENCE), 100, 0, 0, "€", C.FRAIS_AGENCE, _vP, TypeZone.integer),
          Zone("Courtier", _vP.get(C.COURTIER), 100, 0, 0, "€", C.COURTIER, _vP, TypeZone.integer),
          Zone("Autres frais", _vP.get(C.AUTRE_FRAIS_ACHAT), 100, 0, 0, "€", C.AUTRE_FRAIS_ACHAT, _vP, TypeZone.integer),
        ],
      ),
    );
  }
}
