import 'package:flutter/cupertino.dart';
import 'package:jrendement/entities/projet.dart';

import 'package:jrendement/types/constants.dart' as C;
import 'dart:math';

/**
 * Hocine Triki * 2021
 * Provider
 */
class ValueProvider with ChangeNotifier {

  Projet  _projet;

  /**
   * Constructeur,
   * Mise à jour de la map
   */
  ValueProvider(this._projet) {

    _map[C.ACHAT] = this._projet.achat;
    _map[C.LOYER_MENSUEL] = this._projet.loyerMensuel;
    _map[C.VACANCES] = this._projet.vacances;

    _map[C.TRAVAUX] = this._projet.travaux;
    _map[C.FRAIS_AGENCE] = this._projet.fraisAgence;
    _map[C.COURTIER] = this._projet.courtier;
    _map[C.AUTRE_FRAIS_ACHAT] = this._projet.autreFraisAchat;
    _map[C.NOTAIRE] = this._projet.notaire;
    _map[C.MEUBLE] = this._projet.meuble;

    _map[C.TAXE_FONCIERE] = this._projet.taxeFonciere;
    _map[C.ASSURANCE] = this._projet.assurance;
    _map[C.GESTION_LOCATIVE] = this._projet.gestionLocative;
    _map[C.COPROPRIETE] = this._projet.copropriete;
    _map[C.AUTRES_CHARGES] = this._projet.autresCharges;
    _map[C.ENTRETIEN] = this._projet.entretien;
    _map[C.ASSURANCE_IMPAYE] = this._projet.assuranceImpaye;


    _map[C.FI_MONTANT] = this._projet.montantFi;
    _map[C.FI_DUREE] = this._projet.dureeFi;
    _map[C.FI_TAUX] = this._projet.tauxFi;
    _map[C.FI_TAUX_ASS] = this._projet.tauxAssuranceFi;

    _map[C.IMPOT_ANNUEL] = this._projet.impotAnnuel;
    _map[C.DEFISCALISATION] = this._projet.defiscalisation;

  }

  // int _position = 0;    // position du cursor

  Map<String, double> _map = {
      C.ACHAT: 0,
      C.LOYER_MENSUEL: 0,
      C.VACANCES: 0,
      C.TRAVAUX: 0, C.FRAIS_AGENCE: 0, C.COURTIER: 0, C.AUTRE_FRAIS_ACHAT: 0, C.NOTAIRE: 0, C.MEUBLE: 0,
      C.TAXE_FONCIERE: 0, C.ASSURANCE: 0, C.GESTION_LOCATIVE: 0, C.COPROPRIETE:0, C.AUTRES_CHARGES: 0,
      C.ENTRETIEN: 0, C.ASSURANCE_IMPAYE: 0,
      C.IMPOT_ANNUEL: 0, C.DEFISCALISATION: 0,
      C.FI_MONTANT: 0, C.FI_DUREE: 0, C.FI_TAUX: 0, C.FI_TAUX_ASS: 0
  };

  Projet get projet => _projet;
  set projet(Projet value) {_projet = value;}

  set typeImpot(int value) {
    projet.typeImpot = value;
    this.calculateAndNotify();
  }

  bool isCreation() {
    return (this._projet == null || this._projet.id == null);
  }

  get(String key) {
    return _map[key];
  }

  modify(String key, double v) {
    this._map[key] = v;
  }

  modifyAndNotify(String key, double v) {
    this.modify(key, v);
    this.calculateAndNotify();
  }

  addAndNotify(String key, double v) {
    _map[key] = _map[key] + v;
    calculateAndNotify();
  }

  bool isOkBeforeSave() {

    bool isOk = false;

    if (this._projet.nom != null && this._projet.nom.isNotEmpty &&
        _map[C.ACHAT] > 0 &&
        _map[C.LOYER_MENSUEL] > 0) {
      isOk = true;
    }
    return isOk;
  }

  save() {

    this._copyValueToDatabase();
    this.calculate();

    if (this._projet.id == null) {

      Projet.insert(this._projet).then((id) {
        _projet.id = id;
        notifyListeners();
      });
    } else {

      Projet.update(projet).then((count) {
        notifyListeners();
      });
    }
  }

  calculateAndNotify() {
    this.calculate();
    notifyListeners();
  }

  calculate() {

    this.projet.brut = this.projet.net = this.projet.netNet = 0;

    double achat = this._map[C.ACHAT];
    double loyerMensuel = this._map[C.LOYER_MENSUEL];
    double loyerAnnuel = 0;
    double vacancesLocatives = this._map[C.VACANCES];

    if (achat != 0 && loyerMensuel != 0) {

      if (vacancesLocatives != 0) {
        loyerAnnuel = loyerMensuel * 12 * (365 - vacancesLocatives) / 365;
      } else {
        loyerAnnuel = loyerMensuel * 12;
      }
      this.projet.brut = loyerAnnuel * 100 / achat;

      double fraisAcquisition = this._map[C.TRAVAUX] +
          this._map[C.FRAIS_AGENCE] + this._map[C.COURTIER] +
          this._map[C.AUTRE_FRAIS_ACHAT] + this._map[C.NOTAIRE] + this._map[C.MEUBLE];

      double fraisAnnee = this._map[C.TAXE_FONCIERE] + this._map[C.ASSURANCE] +
                          this._map[C.GESTION_LOCATIVE] + this._map[C.AUTRES_CHARGES] +
                          this._map[C.COPROPRIETE] + this._map[C.ENTRETIEN] +
                          this._map[C.ASSURANCE_IMPAYE]; // + this._map[C.IMPOT_ANNUEL];

      // ------------------------------------------------------------------------------------------
      // SECTION CREDIT
      // ------------------------------------------------------------------------------------------
      double montantFi = this._map[C.FI_MONTANT];

      this.projet.mensualiteFi =
          this.projet.montantTotalInteretFi =
          this.projet.montantAssuranceMensuelFi =
          this.projet.montantAssuranceTotalFi =
          this.projet.mensualiteFiToutCompris =
          this.projet.annuiteFiToutCompris =
          this.projet.totalCreditToutCompris = 0;

      if (montantFi > 0) {

        double tFi = this._map[C.FI_TAUX];
        double dureeFi = this._map[C.FI_DUREE];

        if(tFi > 0 && dureeFi > 0) {

          double tauxFi = ((tFi / 100) / 12);
          double numerateur = montantFi * tauxFi;
          double denominateur = 1 - pow((1 + tauxFi), (-12 * dureeFi));

          this.projet.mensualiteFi = numerateur / denominateur;
          this.projet.montantTotalInteretFi = (this.projet.mensualiteFi * dureeFi * 12) - achat;

          double tauxAss = this._map[C.FI_TAUX_ASS];
          if (tauxAss != 0) {
            projet.montantAssuranceMensuelFi = ((tauxAss / 100) * montantFi)  / 12;
            projet.montantAssuranceTotalFi = projet.montantAssuranceMensuelFi * dureeFi * 12;
          }

          projet.mensualiteFiToutCompris = projet.mensualiteFi + projet.montantAssuranceMensuelFi;
          projet.annuiteFiToutCompris = projet.mensualiteFiToutCompris * 12;
          projet.totalCreditToutCompris = projet.montantTotalInteretFi + projet.montantAssuranceTotalFi;
        }
      } else {

        this._map[C.FI_MONTANT] = achat;
      }
      // ------------------------------------------------------------------------------------------
      // ------------------------------------------------------------------------------------------

      double totalFrais = loyerAnnuel - fraisAnnee;

      this.projet.net = totalFrais * 100 / (achat + fraisAcquisition + projet.totalCreditToutCompris);

      // ------------------------------------------------------------------------------------------
      if (this.projet.isImpot()) {

        this.projet.netNet =
            (totalFrais - this._map[C.IMPOT_ANNUEL]) * 100 /
            (achat + fraisAcquisition + projet.totalCreditToutCompris);

        this.projet.cash = (totalFrais) - (projet.mensualiteFiToutCompris * 12) - this._map[C.IMPOT_ANNUEL];

      } else {

        this.projet.netNet =
            (totalFrais + this._map[C.DEFISCALISATION] ) * 100 /
            (achat + fraisAcquisition + projet.totalCreditToutCompris);

        this.projet.cash = (totalFrais + this._map[C.DEFISCALISATION]) - (projet.mensualiteFiToutCompris * 12);
      }

      // ------------------------------------------------------------------------------------------

    }
  }

  _copyValueToDatabase() {

    this.projet.achat = this._map[C.ACHAT];
    this.projet.loyerMensuel = this._map[C.LOYER_MENSUEL];
    this.projet.vacances = this._map[C.VACANCES];

    this.projet.travaux = this._map[C.TRAVAUX];
    this.projet.fraisAgence = this._map[C.FRAIS_AGENCE];
    this.projet.courtier = this._map[C.COURTIER];
    this.projet.autreFraisAchat = this._map[C.AUTRE_FRAIS_ACHAT];
    this.projet.notaire = this._map[C.NOTAIRE];
    this.projet.meuble = this._map[C.MEUBLE];

    this.projet.taxeFonciere = this._map[C.TAXE_FONCIERE];
    this.projet.assurance = this._map[C.ASSURANCE];
    this.projet.gestionLocative = this._map[C.GESTION_LOCATIVE];
    this.projet.copropriete = this._map[C.COPROPRIETE];
    this.projet.autresCharges = this._map[C.AUTRES_CHARGES];

    this.projet.entretien = this._map[C.ENTRETIEN];
    this.projet.assuranceImpaye = this._map[C.ASSURANCE_IMPAYE];
    this.projet.impotAnnuel = this._map[C.IMPOT_ANNUEL];
    this.projet.defiscalisation = this._map[C.DEFISCALISATION];

    this.projet.montantFi = this._map[C.FI_MONTANT];
    this.projet.dureeFi = this._map[C.FI_DUREE];
    this.projet.tauxFi = this._map[C.FI_TAUX];
    this.projet.tauxAssuranceFi = this._map[C.FI_TAUX_ASS];

  }
}