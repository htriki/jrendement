
import 'package:jrendement/helpers/dbProvider.dart';
import 'package:sqflite/sqflite.dart';

final int TYPE_IMPOT_IMPOT = 1;
final int TYPE_IMPOT_DEFISCALISATION = 2;

final String tableProjet = "projet";
// --------------------------------- Identification
final String colId = "id";
final String colNom = "nom";
// --------------------------------- Achat
final String colAchat = "achat";
final String colLoyerMensuel = "loyerMensuel";
final String colVacances = "vacances";
// --------------------------------- Cout d'acquisition
final String colNotaire = "notaire";
final String colTravaux = "travaux";
final String colMeuble = "meuble";
final String colFraisAgence = "fraisAgence";
final String colCourtier = "courtier";
final String colAutreFraisAchat = "autreFraisAchat";
// --------------------------------- Frais annuel
final String colTaxeFonciere = "taxeFonciere";
final String colAssurance = "assurance";
final String colAssuranceImpaye = "assuranceImpaye";
final String colGestionLocative = "gestionLocative";
final String colCopropriete = "copropriete";
final String colEntretien = "entretien";
final String colAutresCharges = "autresCharges";
// --------------------------------- Financement
final String colMontantFi = "montantFi";
final String colTauxFi = "tauxFi";
final String colDureeFi = "dureeFi";
final String colTauxAssuranceFi = "tauxAssuranceFi";

final String colMensualiteFi = "mensualiteFi";
final String colMontantTotalInteretFi = "montantTotalInteretFi";
final String colMontantAssuranceMensuelFi = "montantAssuranceMensuelFi";
final String colMontantAssuranceTotalFi = "montantAssuranceTotalFi";
final String colMensualiteFiToutCompris = "mensualiteFiToutCompris";
final String colAnnuiteFiToutCompris = "annuiteFiToutCompris";
final String colTotalCreditToutCompris = "totalCreditToutCompris";

// --------------------------------- Taux (indicateur)
final String colBrut = "brut";
final String colNet = "net";
final String colNetNet = "netNet";
final String colCash = "cash";
// --------------------------------- Imposition
final String colTypeImpot = "typeImpot";
final String colImpotAnnuel = "impotAnnuel";
final String colDefiscalisation = "defiscalisation";

class Projet {

  // --------------------------------- Identification
  int _id;
  String _nom = "";

  // --------------------------------- Achat
  double _achat = 0;
  double _loyerMensuel = 0;
  double _vacances = 0;

  // --------------------------------- Cout d'acquisition
  double _notaire = 0;
  double _travaux = 0;
  double _meuble = 0;
  double _fraisAgence = 0;
  double _courtier = 0;
  double _autreFraisAchat = 0;

  // --------------------------------- Frais annuel
  double _taxeFonciere = 0;
  double _assurance = 0;
  double _assuranceImpaye = 0;
  double _gestionLocative = 0;
  double _copropriete = 0;
  double _entretien = 0;
  double _autresCharges = 0;

  // --------------------------------- Financement
  double _montantFi = 0;
  double _tauxFi = 0;
  double _dureeFi = 0;
  double _tauxAssuranceFi = 0;

  double _mensualiteFi = 0;
  double _montantTotalInteretFi = 0;
  double _montantAssuranceMensuelFi = 0;
  double _montantAssuranceTotalFi = 0;
  double _mensualiteFiToutCompris = 0;
  double _annuiteFiToutCompris = 0;
  double _totalCreditToutCompris = 0;
  // --------------------------------- Taux (indicateur)
  double _brut = 0;
  double _net = 0;
  double _netNet = 0;
  double _cash = 0;
  // --------------------------------- Imposition
  int _typeImpot = TYPE_IMPOT_IMPOT;
  double _impotAnnuel = 0;
  double _defiscalisation = 0;

  Projet();

  Projet.light(this._id);

  // Constructeur
  Projet.complete(
      // --------------------------------- Identification
      this._id,
      this._nom,

      // --------------------------------- Achat
      this._achat,
      this._loyerMensuel,
      this._vacances,

      // --------------------------------- Cout d'acquisition
      this._notaire,
      this._travaux,
      this._meuble,
      this._fraisAgence,
      this._courtier,
      this._autreFraisAchat,

      // --------------------------------- Frais Annuel
      this._taxeFonciere,
      this._assurance,
      this._assuranceImpaye,
      this._gestionLocative,
      this._copropriete,
      this._entretien,
      this._autresCharges,
      // --------------------------------- Financement
      this._montantFi,
      this._tauxFi,
      this._dureeFi,
      this._tauxAssuranceFi,
      this._mensualiteFi,
      this._montantTotalInteretFi,
      this._montantAssuranceMensuelFi,
      this._montantAssuranceTotalFi,
      this._mensualiteFiToutCompris,
      this._annuiteFiToutCompris,
      this._totalCreditToutCompris,

      // --------------------------------- Taux (indicateur)
      this._brut,
      this._net,
      this._netNet,
      this._cash,

      // --------------------------------- Imposition
      this._typeImpot,
      this._impotAnnuel,
      this._defiscalisation,);

  //lecture
  Map<String, dynamic> toMap() {
    return {
      // --------------------------------- Identification
      'id': _id,
      'nom': _nom,
      // --------------------------------- Achat
      'achat': _achat,
      'loyerMensuel': _loyerMensuel,
      'vacances': _vacances,
      // --------------------------------- Cout d'acquisition
      'notaire': _notaire,
      'travaux': _travaux,
      'meuble': _meuble,
      'fraisAgence': _fraisAgence,
      'courtier': _courtier,
      'autreFraisAchat': _autreFraisAchat,
      // --------------------------------- Frais Annuel
      'taxeFonciere': _taxeFonciere,
      'assurance': _assurance,
      'assuranceImpaye': _assuranceImpaye,
      'gestionLocative': _gestionLocative,
      'copropriete': _copropriete,
      'entretien': _entretien,
      'autresCharges': _autresCharges,
      // --------------------------------- Financement
      'montantFi': _montantFi,
      'tauxFi': _tauxFi,
      'dureeFi': _dureeFi,
      'tauxAssuranceFi': _tauxAssuranceFi,
      'mensualiteFi': _mensualiteFi,
      'montantTotalInteretFi': _montantTotalInteretFi,
      'montantAssuranceMensuelFi': _montantAssuranceMensuelFi,
      'montantAssuranceTotalFi': _montantAssuranceTotalFi,
      'mensualiteFiToutCompris': _mensualiteFiToutCompris,
      'annuiteFiToutCompris': _annuiteFiToutCompris,
      'totalCreditToutCompris': _totalCreditToutCompris,
      // --------------------------------- Taux (indicateur)
      'brut': _brut,
      'net': _net,
      'netNet': _netNet,
      'cash': _cash,
      // --------------------------------- Imposition
      'typeImpot': _typeImpot,
      'impotAnnuel': _impotAnnuel,
      'defiscalisation': _defiscalisation,
    };
  }

  Projet.fromMap(Map<String, dynamic> map) {
    // --------------------------------- Identification
    _id = map[colId];
    _nom = map[colNom];
    // --------------------------------- Achat
    _achat = map[colAchat];
    _loyerMensuel = map[colLoyerMensuel];
    _vacances = map[colVacances];
    // --------------------------------- Cout d'acquisition
    _notaire = map[colNotaire];
    _travaux = map[colTravaux];
    _meuble = map[colMeuble];
    _fraisAgence = map[colFraisAgence];
    _courtier = map[colCourtier];
    _autreFraisAchat = map[colAutreFraisAchat];
    // --------------------------------- Frais Annuel
    _taxeFonciere = map[colTaxeFonciere];
    _assurance = map[colAssurance];
    _assuranceImpaye = map[colAssuranceImpaye];
    _gestionLocative = map[colGestionLocative];
    _copropriete = map[colCopropriete];
    _entretien = map[colEntretien];
    _autresCharges = map[colAutresCharges];
    // --------------------------------- Financement
    _montantFi = map[colMontantFi];
    _tauxFi = map[colTauxFi];
    _dureeFi = map[colDureeFi];
    _tauxAssuranceFi = map[colTauxAssuranceFi];
    _mensualiteFi = map[colMensualiteFi];
    _montantTotalInteretFi = map[colMontantTotalInteretFi];
    _montantAssuranceMensuelFi = map[colMontantAssuranceMensuelFi];
    _montantAssuranceTotalFi = map[colMontantAssuranceTotalFi];
    _mensualiteFiToutCompris = map[colMensualiteFiToutCompris];
    _annuiteFiToutCompris = map[colAnnuiteFiToutCompris];
    _totalCreditToutCompris = map[colTotalCreditToutCompris];
    // --------------------------------- Taux (indicateur)
    _brut = map[colBrut];
    _net = map[colNet];
    _netNet = map[colNetNet];
    _cash = map[colCash];
    // --------------------------------- Imposition
    _typeImpot = map[colTypeImpot];
    _impotAnnuel = map[colImpotAnnuel];
    _defiscalisation = map[colDefiscalisation];

  }

  static Future<int> insert(Projet projet) async {

    Database db = await DbProvider.instance.db;
    int id = await db.insert(tableProjet, projet.toMap());
    return id;
  }

  static Future<int> update(Projet projet) async {

    Database db = await DbProvider.instance.db;
    int updateCount = await db.update(
        tableProjet,
        projet.toMap(),
        where: '$colId = ?',
        whereArgs: [projet.id]);
    return updateCount;
  }

  static Future<int> count() async {
    Database db = await DbProvider.instance.db;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tableProjet'));
  }

  static Future<List<Projet>> findAll() async {

    Database db = await DbProvider.instance.db;
    List<Map<String, dynamic>> maps = await db.query(tableProjet,
        columns: [
          colId, colNom,
          colAchat, colLoyerMensuel, colVacances,
          colNotaire, colTravaux, colMeuble, colFraisAgence, colCourtier, colAutreFraisAchat,
          colTaxeFonciere, colAssurance, colAssuranceImpaye, colGestionLocative, colCopropriete, colEntretien, colAutresCharges,
          colMontantFi, colTauxFi, colDureeFi, colTauxAssuranceFi,
          colMensualiteFi, colMontantTotalInteretFi, colMontantAssuranceMensuelFi, colMontantAssuranceTotalFi,
          colMensualiteFiToutCompris, colAnnuiteFiToutCompris, colTotalCreditToutCompris,
          colBrut, colNet, colNetNet, colCash,
          colTypeImpot, colImpotAnnuel, colDefiscalisation,
        ]);

    if (maps.length > 0) {

      return List.generate(maps.length, (i) {
        return Projet.complete(
            maps[i][colId], maps[i][colNom],
            maps[i][colAchat], maps[i][colLoyerMensuel], maps[i][colVacances],
            maps[i][colNotaire], maps[i][colTravaux], maps[i][colMeuble], maps[i][colFraisAgence], maps[i][colCourtier], maps[i][colAutreFraisAchat],
            maps[i][colTaxeFonciere], maps[i][colAssurance], maps[i][colAssuranceImpaye], maps[i][colGestionLocative], maps[i][colCopropriete], maps[i][colEntretien], maps[i][colAutresCharges],
            maps[i][colMontantFi], maps[i][colTauxFi], maps[i][colDureeFi], maps[i][colTauxAssuranceFi],
            maps[i][colMensualiteFi], maps[i][colMontantTotalInteretFi], maps[i][colMontantAssuranceMensuelFi],
            maps[i][colMontantAssuranceTotalFi], maps[i][colMensualiteFiToutCompris],
            maps[i][colAnnuiteFiToutCompris], maps[i][colTotalCreditToutCompris],
            maps[i][colBrut], maps[i][colNet], maps[i][colNetNet], maps[i][colCash],
            maps[i][colTypeImpot], maps[i][colImpotAnnuel], maps[i][colDefiscalisation],
        );
      });
    }
    return null;
  }

  static Future<Projet> findById(int id) async {

    Database db = await DbProvider.instance.db;
    List<Map> maps = await db.query(tableProjet,
        columns: [
          colId, colNom,
          colAchat, colLoyerMensuel, colVacances,
          colNotaire, colTravaux, colMeuble, colFraisAgence, colCourtier, colAutreFraisAchat,
          colTaxeFonciere, colAssurance, colAssuranceImpaye, colGestionLocative, colCopropriete, colEntretien, colAutresCharges,
          colMontantFi, colTauxFi, colDureeFi, colTauxAssuranceFi,
          colMensualiteFi, colMontantTotalInteretFi, colMontantAssuranceMensuelFi, colMontantAssuranceTotalFi,
          colMensualiteFiToutCompris, colAnnuiteFiToutCompris, colTotalCreditToutCompris,
          colBrut, colNet, colNetNet, colCash,
          colTypeImpot, colImpotAnnuel, colDefiscalisation,
        ],
        where: '$colId = ?',
        whereArgs: [id]);

    if (maps.length > 0) {
      return Projet.fromMap(maps.first);
    }
    return null;
  }

  static Future<int> delete(int id) async {
    Database db = await DbProvider.instance.db;
    return await db
        .delete(tableProjet, where: '$colId = ?', whereArgs: [id]);
  }

  static Future<void> deleteAll() async {
    Database db = await DbProvider.instance.db;
    await db.delete(tableProjet);
  }

  bool isImpot() => (_typeImpot == TYPE_IMPOT_IMPOT);

  double get defiscalisation => _defiscalisation;

  set defiscalisation(double value) {
    _defiscalisation = value;
  }

  double get impotAnnuel => _impotAnnuel;

  set impotAnnuel(double value) {
    _impotAnnuel = value;
  }

  int get typeImpot => _typeImpot;

  set typeImpot(int value) {
    _typeImpot = value;
  }

  double get cash => _cash;

  set cash(double value) {
    _cash = value;
  }

  double get netNet => _netNet;

  set netNet(double value) {
    _netNet = value;
  }

  double get net => _net;

  set net(double value) {
    _net = value;
  }

  double get brut => _brut;

  set brut(double value) {
    _brut = value;
  }

  double get totalCreditToutCompris => _totalCreditToutCompris;

  set totalCreditToutCompris(double value) {
    _totalCreditToutCompris = value;
  }

  double get annuiteFiToutCompris => _annuiteFiToutCompris;

  set annuiteFiToutCompris(double value) {
    _annuiteFiToutCompris = value;
  }

  double get mensualiteFiToutCompris => _mensualiteFiToutCompris;

  set mensualiteFiToutCompris(double value) {
    _mensualiteFiToutCompris = value;
  }

  double get montantAssuranceTotalFi => _montantAssuranceTotalFi;

  set montantAssuranceTotalFi(double value) {
    _montantAssuranceTotalFi = value;
  }

  double get montantAssuranceMensuelFi => _montantAssuranceMensuelFi;

  set montantAssuranceMensuelFi(double value) {
    _montantAssuranceMensuelFi = value;
  }

  double get montantTotalInteretFi => _montantTotalInteretFi;

  set montantTotalInteretFi(double value) {
    _montantTotalInteretFi = value;
  }

  double get mensualiteFi => _mensualiteFi;

  set mensualiteFi(double value) {
    _mensualiteFi = value;
  }

  double get tauxAssuranceFi => _tauxAssuranceFi;

  set tauxAssuranceFi(double value) {
    _tauxAssuranceFi = value;
  }

  double get dureeFi => _dureeFi;

  set dureeFi(double value) {
    _dureeFi = value;
  }

  double get tauxFi => _tauxFi;

  set tauxFi(double value) {
    _tauxFi = value;
  }

  double get montantFi => _montantFi;

  set montantFi(double value) {
    _montantFi = value;
  }

  double get autresCharges => _autresCharges;

  set autresCharges(double value) {
    _autresCharges = value;
  }

  double get entretien => _entretien;

  set entretien(double value) {
    _entretien = value;
  }

  double get copropriete => _copropriete;

  set copropriete(double value) {
    _copropriete = value;
  }

  double get gestionLocative => _gestionLocative;

  set gestionLocative(double value) {
    _gestionLocative = value;
  }

  double get assuranceImpaye => _assuranceImpaye;

  set assuranceImpaye(double value) {
    _assuranceImpaye = value;
  }

  double get assurance => _assurance;

  set assurance(double value) {
    _assurance = value;
  }

  double get taxeFonciere => _taxeFonciere;

  set taxeFonciere(double value) {
    _taxeFonciere = value;
  }

  double get autreFraisAchat => _autreFraisAchat;

  set autreFraisAchat(double value) {
    _autreFraisAchat = value;
  }

  double get courtier => _courtier;

  set courtier(double value) {
    _courtier = value;
  }

  double get fraisAgence => _fraisAgence;

  set fraisAgence(double value) {
    _fraisAgence = value;
  }

  double get meuble => _meuble;

  set meuble(double value) {
    _meuble = value;
  }

  double get travaux => _travaux;

  set travaux(double value) {
    _travaux = value;
  }

  double get notaire => _notaire;

  set notaire(double value) {
    _notaire = value;
  }

  double get vacances => _vacances;

  set vacances(double value) {
    _vacances = value;
  }

  double get loyerMensuel => _loyerMensuel;

  set loyerMensuel(double value) {
    _loyerMensuel = value;
  }

  double get achat => _achat;

  set achat(double value) {
    _achat = value;
  }

  String get nom => _nom;

  set nom(String value) {
    _nom = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

// Génération Getter / Setter


}